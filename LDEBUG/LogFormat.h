#ifndef LOG_FORMAT_H
#define LOG_FORMAT_H

//These define names must not change
//the log parser relies on this

#define TRACE_PROLOGUE "Enter"
#define TRACE_EPILOGUE "Leave"

#define LOG_SEVERITY "%-6s"
#define LOG_THREAD "T[%lu]"
#define LOG_FILE "%s[%d]"
#define LOG_FUNCTION "%s"
#define LOG_DELIMITER " :: "
#define LOG_TEMPLATE LOG_SEVERITY LOG_DELIMITER LOG_THREAD LOG_DELIMITER LOG_FILE LOG_DELIMITER LOG_FUNCTION LOG_DELIMITER

#endif /* LOG_FORMAT_H */