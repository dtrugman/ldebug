#include <cstdarg>

#include "Logger.h"
#include "Platform.h"

namespace ldebug
{

Logger Logger::_instance;

const char * Logger::OVERRIDE_MODE = "w";
const char * Logger::APPEND_MODE = "a+";
const Logger::Severity Logger::DEFAULT_THRESHOLD = Logger::Trace;
const char * Logger::SEVERITY_STRINGS[] = { "Emerg", "Alert", "Crit", "Error", "Warn", "Notice", "Info", "Debug", "Trace" };

Logger::Tracer::Tracer(const char * file, int line, const char * func)
	: _file(file), _line(line), _func(func)
{
	LOG__(Trace, LOG_TEMPLATE TRACE_PROLOGUE "\n",
		SEVERITY_STRINGS[Trace], Platform::getThreadId(), _file, _line, _func);
}

Logger::Tracer::~Tracer(void)
{
	LOG__(Trace, LOG_TEMPLATE TRACE_EPILOGUE "\n",
		SEVERITY_STRINGS[Trace], Platform::getThreadId(), _file, _line, _func);
}


Logger::Logger(void)
{
	_threshold = DEFAULT_THRESHOLD;
}

Logger::~Logger(void)
{
}

int Logger::open(const char * path, int append)
{
	int ret = 0;

	if(_opened == 0)
	{
		const char * mode = append ? APPEND_MODE : OVERRIDE_MODE;
		if(fopen_s(&_fd, path, mode) == 0)
		{
			_opened = 1;
		}
		else
		{
			ret = -1;
		}
	}

	return ret;
}

void Logger::close()
{
	if(_fd != NULL)
	{
		fclose(_fd);
		_fd = NULL;
		_opened = 0;
	}
}

Logger::Severity Logger::getThreshold()
{
	return _threshold;
}

int Logger::setThreshold(Logger::Severity threshold)
{
	int ret = -1;

	if(threshold > 0 && threshold < Max)
	{
		_threshold = threshold;
		ret = 0;
	}

	return ret;
}

void Logger::log(Severity severity, const char * format, ...)
{
	if(severity <= _threshold)
	{
		va_list va;
		va_start(va, format);

		vfprintf(_fd, format, va);

		va_end(va);
	}
}

} //namespace ldebug