#ifndef LOGGER_H
#define LOGGER_H

#include <stdio.h>

#include "LogFormat.h"

namespace ldebug
{

#define LOG(severity, format, ...) \
	LOG__(severity, LOG_TEMPLATE format "\n", Logger::SEVERITY_STRINGS[severity], \
	Platform::getThreadId(), __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__)

#define LOG__(severity, format, ...) Logger::inst().log(severity, format, ##__VA_ARGS__)

#define TRACE() Logger::Tracer __tracer(__FILE__, __LINE__, __FUNCTION__)

class Logger
{
public:
	class Tracer
	{	
	public:
		Tracer(const char * file, int line, const char * func);
		~Tracer(void);

	private:
		const char * _file;
		const char * _func;
		int _line;
	};

	enum Severity
	{
		Min = -1,
		Emerg,		/* system is unusable */
		Alert,		/* action must be taken immediately */
		Critical,	/* critical conditions */
		Error,		/* error conditions */
		Warning,	/* warning conditions */
		Notice,		/* normal but significant condition */
		Info,		/* informational */
		Debug,		/* debug-level messages */
		Trace,		/* trace-level messages */
		Max
	};

	~Logger(void);

	static const char * SEVERITY_STRINGS[];

	static Logger& inst() { return _instance; };

	int open(const char * path, int append = 0);
	void close();

	Severity getThreshold();
	int setThreshold(Severity threshold);

	void log(Severity severity, const char * format, ...);

private:
	Logger(void);

	static const char * OVERRIDE_MODE;
	static const char * APPEND_MODE;
	static const Severity DEFAULT_THRESHOLD;

	static Logger _instance;

	FILE * _fd;
	int _opened;
	Severity _threshold;
};

} //namespace ldebug

#endif /* LOGGER_H */