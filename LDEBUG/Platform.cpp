#include "Platform.h"

namespace ldebug
{

unsigned long Platform::Infinity = INFINITE;

unsigned long Platform::getThreadId()
{
	return GetCurrentThreadId();
}

Platform::ThreadHandle Platform::spawnThread(ThreadRoutine routine, void * args)
{
	return CreateThread(NULL,								//No attributes
						0,									//Default stack size
						(LPTHREAD_START_ROUTINE)routine,	//Routine
						args,								//Routine arguments
						0,									//Run immediately
						NULL);								//Don't request thread ID
}

int Platform::waitOnThread(Platform::ThreadHandle handle, unsigned long ms)
{
	int retval = -1;

	DWORD result = WaitForSingleObject(handle, Infinity);
	if(result == WAIT_OBJECT_0)
	{
		retval = 0;
	}
	else if(result == WAIT_TIMEOUT)
	{
		retval = 1;
	}

	return retval;
}

} //namespace ldebug