#ifndef PLATFORM_H
#define PLATFORM_H

#include <Windows.h>

namespace ldebug
{

class Platform
{
public:
	typedef unsigned long (* ThreadRoutine)(void * args);
	typedef void * ThreadHandle;

	static unsigned long Infinity;

	static unsigned long getThreadId();
	static ThreadHandle spawnThread(ThreadRoutine routine, void * args = NULL);
	static int waitOnThread(ThreadHandle handle, unsigned long ms = Infinity);

private:
	Platform(void) {}
	~Platform(void) {}
};

} //namespace ldebug

#endif /* PLATFORM_H */