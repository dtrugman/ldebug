#include <stdio.h>
#include <iostream>
#include <cstdarg>

#include "Logger.h"
#include "Platform.h"

using namespace ldebug;

typedef struct
{
	int a;
	int b;
}WorkerArgs;

void step4()
{
	TRACE();

	LOG(Logger::Info, "This is an error message: %s", "Please take measures");
}

void step3()
{
	TRACE();

	LOG(Logger::Info, "This is an info message: %s", "Please pay attention");
}

unsigned long step2(void * a)
{
	WorkerArgs * wa = (WorkerArgs *)a;

	TRACE();

	for(int i = 0; i < wa->a; i++)
	{
		step4();
	}

	free(wa);

	return 0;
}

unsigned long step1(void * a)
{
	WorkerArgs * wa = (WorkerArgs *)a;

	TRACE();

	for(int i = 0; i < wa->a; i++)
	{
		step3();
	}

	free(wa);

	return -1;
}

void run()
{
	TRACE();

	WorkerArgs * a1 = (WorkerArgs *)malloc(sizeof(*a1));
	if(a1 == NULL)
	{
		return;
	}

	WorkerArgs * a2 = (WorkerArgs *)malloc(sizeof(*a2));
	if(a2 == NULL)
	{
		free(a1);
		return;
	}

	a1->a = 3;
	a2->a = 5;

	Platform::ThreadHandle t1 = Platform::spawnThread(step1, a1);
	if(t1 != 0)
	{
		LOG(Logger::Info, "Spawned t1 successfully!");
	}

	Platform::ThreadHandle t2 = Platform::spawnThread(step2, a2);
	if(t2 != 0)
	{
		LOG(Logger::Info, "Spawned t2 successfully!");
	}

	Platform::waitOnThread(t1);
	Platform::waitOnThread(t2);
}

int main(int argc, char ** argv)
{
	if(Logger::inst().open("log.txt") >= 0)
	{
		run();

		Logger::inst().close();
	}

	return 0;
}