import sys
import traceback

READ_MODE 	= 'r'

PROLOGUE 	= "TRACE_PROLOGUE"
EPILOGUE 	= "TRACE_EPILOGUE"
TEMPLATE 	= "LOG_TEMPLATE"
SEVERITY 	= "LOG_SEVERITY"
THREAD 		= "LOG_THREAD"
FILE 		= "LOG_FILE"
FUNCTION 	= "LOG_FUNCTION"
DELIMITER 	= "LOG_DELIMITER"
MESSAGE 	= "LOG_MESSAGE"

FORMATS = [ PROLOGUE, EPILOGUE, TEMPLATE, SEVERITY, 
	THREAD, FILE, FUNCTION, DELIMITER ]

PARTS = [ SEVERITY, THREAD, FILE, FUNCTION, MESSAGE ]

def readLogFile(path):
	raw_log = []
	with open(path, READ_MODE) as file:
		for line in file:
			raw_log.append(line.strip())

	return raw_log


def readFormatsFile(path):
	formats = {}
	with open(path, READ_MODE) as file:
		for line in file:
			for key in FORMATS:
				define = "#define " + key
				pos = line.find(define)
				if pos != -1:
					value = line[pos+len(define)+1:].strip()
					if key in formats:
						raise Exception("Found a duplicate format(" + key + ") in " + path)				
					else:
						if(value[0] == '"'): # Trim "" if they surround the value
							value = value[1:-1]

						formats[key] = value.strip()

	if len(formats) != len(FORMATS):
		raise Exception("Couldn't find all required formats in " + path)

	return formats


def parseFormat(formats):
	temp = formats[TEMPLATE].split(DELIMITER); # Split by delimiter
	temp = filter(None, temp) # Remove empty strings
	temp_len = len(temp)
	for i in range(temp_len): # Strip each and every string
		temp[i] = temp[i].strip()
	temp.append(MESSAGE) # A trick to avoid abnormalities in the next loop

	indices = {}
	for p in PARTS:
		indices[p] = temp.index(p)

	return indices


def parseLog(raw_log, formats, indices):
	parsed_log = []
	for line in raw_log:
		line_parts = line.split(formats[DELIMITER])
		parsed_line = {}
		for p in PARTS:
			parsed_line[p] = line_parts[indices[p]]
		parsed_log.append(parsed_line)

	return parsed_log
		

def printFormat(raw_log, formats, indices, parsed_log):	
	print "---------------------------------------"
	print "Raw log:"
	print "---------------------------------------"
	for line in raw_log:
		print line

	print "---------------------------------------"
	print "Formats"
	print "---------------------------------------"
	for key in formats:
		print key + ": " + formats[key]

	print "---------------------------------------"
	print "Template"
	print "---------------------------------------"
	print "Severity @ " + str(indices[SEVERITY])
	print "Thread @ " 	+ str(indices[THREAD])
	print "File @ " 	+ str(indices[FILE])
	print "Function @ " + str(indices[FUNCTION])
	print "Message @ " 	+ str(indices[MESSAGE])

	print "---------------------------------------"
	print "Parsed log:"
	print "---------------------------------------"
	i = 0;
	for parsed_line in parsed_log:
		print "Line " + str(i) + ":"
		for key in parsed_line:
			print key + ":\t" + parsed_line[key]
		print "-------------"
		i += 1

if __name__ == "__main__":
	try:
		raw_log = readLogFile(sys.argv[1])
		formats = readFormatsFile(sys.argv[2])
		indices = parseFormat(formats)
		parsed_log = parseLog(raw_log, formats, indices)

		printFormat(raw_log, formats, indices, parsed_log)

		print "Successful!"
	except:
		traceback.print_exc(file=sys.stderr)
		print "Terminating!"